﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Navegacion
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            NavigationPage paginaNavegacion = new NavigationPage(new PaginaNavegacion());
            Application.Current.MainPage = paginaNavegacion;
            
        }
    }
}
